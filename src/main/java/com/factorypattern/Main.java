package com.factorypattern;

import com.factorypattern.view.Menu;

public class Main {

    public static void main(final String[] args) {
        new Menu().show();
    }
}

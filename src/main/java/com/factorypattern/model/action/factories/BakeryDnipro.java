package com.factorypattern.model.action.factories;

import com.factorypattern.model.action.pizzaactions.PizzaAction;
import com.factorypattern.model.action.pizzaactions.impl.CheesePizzaAction;
import com.factorypattern.model.action.pizzaactions.impl.MargaritaPizzaAction;
import com.factorypattern.model.action.pizzaactions.impl.PepperoniPizzaAction;
import com.factorypattern.model.action.pizzaactions.impl.SeafoodPizzaAction;
import com.factorypattern.model.entity.condition.Condition;

public class BakeryDnipro extends Bakery {
    @Override
    protected PizzaAction getPizzaLogic(Condition condition) {
        PizzaAction pizza = null;
        if (condition == condition.PEPPERONI) {
            //added some new actions from cook
            PepperoniPizzaAction action = new PepperoniPizzaAction();
            action.prepare();
            return action;
        }
        if (condition == condition.CHEESE) {
            return new CheesePizzaAction();
        }
        if (condition == condition.VEGIE) {
            LOGGER.info("Sorry, but veggie pizza is missing in Dnipro.");
        }
        if (condition == condition.SEAFOOD) {
            return new SeafoodPizzaAction();
        }
        if (condition == condition.MARGARITA) {
            return new MargaritaPizzaAction();
        }
        return pizza;
    }
}

package com.factorypattern.model.action.factories;

import com.factorypattern.model.action.pizzaactions.PizzaAction;
import com.factorypattern.model.entity.condition.Condition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class Bakery {
    protected abstract PizzaAction getPizzaLogic(Condition condition);
    protected final Logger LOGGER = LogManager.getLogger(Bakery.class);

    public PizzaAction getPizzaLogicInstance(Condition condition) {
        PizzaAction pizza_baked = getPizzaLogic(condition);
        if(pizza_baked == null) {
            return null;
        }
        pizza_baked.prepare();
        pizza_baked.bake();
        pizza_baked.cut();
        pizza_baked.box();
        return pizza_baked;
    }
}

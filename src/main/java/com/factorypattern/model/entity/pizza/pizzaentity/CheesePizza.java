package com.factorypattern.model.entity.pizza.pizzaentity;


import com.factorypattern.model.entity.pizza.pizzaenum.Dough;
import com.factorypattern.model.entity.pizza.pizzaenum.Sauce;
import com.factorypattern.model.entity.pizza.pizzaenum.Toppings;

import java.util.ArrayList;
import java.util.List;

public class CheesePizza extends Pizza {
    public CheesePizza() {
        super("Cheese", Dough.THIN, Sauce.WHITE, new ArrayList<Toppings>());
        this.setToppings(getStandardToppings());
    }

    public CheesePizza(String type, Dough dough, Sauce sauce,
                       ArrayList<Toppings> toppings) {
        super(type, dough, sauce, toppings);
    }

    private List<Toppings> getStandardToppings() {
        List<Toppings> toppings = new ArrayList<>();
        toppings.add(Toppings.PARMESAN);
        toppings.add(Toppings.MOZZARELLA);
        toppings.add(Toppings.CHEDDAR);
        toppings.add(Toppings.DORBLU);
        return toppings;
    }
}

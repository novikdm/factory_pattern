package com.factorypattern.model.entity.pizza.pizzaentity;


import com.factorypattern.model.entity.pizza.pizzaenum.Dough;
import com.factorypattern.model.entity.pizza.pizzaenum.Sauce;
import com.factorypattern.model.entity.pizza.pizzaenum.Toppings;

import java.util.ArrayList;
import java.util.List;

public class MargaritaPizza extends Pizza {
    public MargaritaPizza() {
        super("Margarita", Dough.THIN, Sauce.TOMATO, new ArrayList<Toppings>());
        this.setToppings(getStandardToppings());
    }

    public MargaritaPizza(String type, Dough dough, Sauce sauce,
                          ArrayList<Toppings> toppings) {
        super(type, dough, sauce, toppings);
    }

    private List<Toppings> getStandardToppings() {
        List<Toppings> toppings = new ArrayList<>();
        toppings.add(Toppings.TOMATO);
        toppings.add(Toppings.MOZZARELLA);
        toppings.add(Toppings.BASIL);
        return toppings;
    }
}

package com.factorypattern.view.interfaces;

@FunctionalInterface
public interface Printable {
    void print();
}

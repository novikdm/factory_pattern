package com.factorypattern.view;

public final class Menu extends AbstractMenu {

    public Menu() {
        menu.put(0, "Order pizza");
        menu.put(1, "Exit");
        methodsForMenu.put(0, this::showPizzaOrdering);
        methodsForMenu.put(1, this::quit);
    }

    public void show() {
        out();
    }

    @Override
    protected void showInfo() {
        LOGGER.info("Menu for factory_Pattern");
        menu.forEach((key, elem) -> LOGGER.info(key + " : " + elem));
    }

    private void showPizzaOrdering() {
        LOGGER.info("Please, input the name of pizza");
        String pizzaName = scan.next();
        LOGGER.info("Please, input the name of town");
        String townName = scan.next();
        String pizza = CONTROLLER.getPizza(pizzaName, townName);
        if (pizza == null) {
            badInput();
            return;
        }
        LOGGER.info(pizza);
    }
}
